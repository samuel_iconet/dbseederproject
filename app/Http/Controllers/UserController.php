<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    //
    public function getUsers(){
        $users = User::paginate(1000);
        $response['code'] = 200;
        $response['users'] = $users;
        return response()->json($response , 200); 
    }
}
